<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 04:58
 */

use PHPUnit\Framework\TestCase;
use Anarchalien\Checkout\Factories\ProductFactory;
/**
 * Class ProductFactoryTest
 */
class ProductFactoryTest extends TestCase
{
    protected $factory;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->factory = ProductFactory::getProducts();
    }

    public function testFactory()
    {
        $this->assertArrayHasKey(0,$this->factory);

        $this->assertInstanceOf(\Anarchalien\Checkout\Interfaces\ProductInterface::class,$this->factory[0]);
    }
}