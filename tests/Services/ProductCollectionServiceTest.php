<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 05:08
 */

use PHPUnit\Framework\TestCase;
use Anarchalien\Checkout\Factories\ProductFactory;
use Anarchalien\Checkout\Services\ProductCollectionService;
use Anarchalien\Checkout\Interfaces\ProductInterface;

/**
 * Class ProductCollectionServiceTest
 */
class ProductCollectionServiceTest extends TestCase
{
    protected $service;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->service = (new ProductCollectionService(new ProductFactory()));
    }

    public function testCollectionServiceReturnsProducts()
    {
        $results = $this->service->all();

        $this->assertArrayHasKey(0,$results);
        $this->assertInstanceOf(ProductInterface::class,$results[0]);
    }

    public function testSearchBySku()
    {
        $results = $this->service->findBySku('A');

        $this->assertArrayHasKey(0,$results);
        $this->assertInstanceOf(ProductInterface::class,$results[0]);
        $this->assertEquals('A',$results[0]->getSku());
    }

    public function testFailedSearch()
    {
        $results = $this->service->findBySku('Z');

        $this->assertArrayNotHasKey(0,$results);
    }
}