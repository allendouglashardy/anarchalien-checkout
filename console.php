<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 06:53
 */
/** console.php **/
#!/usr/bin/env php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Anarchalien\Checkout\Console;

try {
    $app = new Application();
    $app->add(new Console());
    $app->run();
}
catch(Exception $exception){
    echo $exception->getMessage();
}