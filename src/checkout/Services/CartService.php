<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 05:20
 */

namespace Anarchalien\Checkout\Services;


use Anarchalien\Checkout\Interfaces\CartInterface;
use Anarchalien\Checkout\Interfaces\ProductCollectionInterface;
/**
 * Class CartService
 * @package Anarchalien\Checkout\Services
 */
class CartService implements CartInterface
{
    /**
     * @var ProductCollectionInterface
     */
    protected $collection;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var array
     */
    protected $bill = [];

    public function __construct(ProductCollectionInterface $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function getBill() : array
    {
        return $this->bill;
    }

    public function clear():CartInterface
    {
        $this->items = [];

        return $this;
    }

    /**
     * @return float
     */
    public function getTotals(): float
    {
        $total = 0;

        if(!empty($this->items)){
            foreach ($this->items as $key=>$quantity){
                $product = $this->collection->findBySku($key);
                $priceForOne = $product[0]->getPrice();

                $totalQuantity = $quantity;

                if($product[0]->hasActiveDiscount()==false||
                    ($product[0]->hasActiveDiscount()==true && $totalQuantity<$product[0]->getDiscountThreshold())) {
                        $lineTotal = $priceForOne * $quantity;

                        $this->bill[] = [
                            'sku'=>$key,
                            'quantity'=>$quantity,
                            'lineTotal'=>$lineTotal
                        ];
                }else{
                    $remainderFromMod = $totalQuantity % $product[0]->getDiscountThreshold();
                    $quantityToDiscount = $totalQuantity-$remainderFromMod;

                    $lineTotal = $remainderFromMod * $priceForOne;

                    if($remainderFromMod>0){
                        $this->bill[] = [
                            'sku'=>$key,
                            'quantity'=>$remainderFromMod,
                            'lineTotal'=>$lineTotal
                        ];
                    }

                    $discountedTotal = ($quantityToDiscount * $priceForOne) - ($product[0]->getDiscountAmount()*(
                        $quantityToDiscount/$product[0]->getDiscountThreshold()
                            ));

                    $this->bill[] = [
                        'sku'=>$key,
                        'quantity'=>$quantityToDiscount,
                        'lineTotal'=>$discountedTotal
                    ];

                    $lineTotal+=$discountedTotal;
                }

                $total+=$lineTotal;
            }
        }

        $this->bill[] = ['Label'=>'Total','-'=>'-','total'=>floatval($total)];

        return floatval($total);
    }

    /**
     * @param string $sku
     * @param int $quantity
     * @return CartInterface
     */
    public function addItem(string $sku, int $quantity = 1): CartInterface
    {
        $products = $this->collection->findBySku($sku);

        if(!empty($products)){
            $this->items[$sku] = (isset($this->items[$sku])) ? $this->items[$sku]+=$quantity : $quantity;
        }

        return $this;
    }
}