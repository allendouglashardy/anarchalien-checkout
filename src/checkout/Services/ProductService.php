<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 31/05/18
 * Time: 16:58
 */

namespace Anarchalien\Checkout\Services;


use Anarchalien\Checkout\Exceptions\IllegalNegativeValueException;
use Anarchalien\Checkout\Exceptions\ProductDoesNotHaveSkuException;
use Anarchalien\Checkout\Interfaces\ProductInterface;

/**
 * Class ProductService
 * @package Anarchalien\Checkout\Services
 */
class ProductService implements ProductInterface
{
    /**
     * @var float
     */
    protected $price = 0.00;

    /**
     * @var string|bool
     */
    protected $sku = false;

    /**
     * @var bool
     */
    protected $activeDiscount = false;

    /**
     * @var $quantity
     */
    protected $quantity;

    /**
     * @var int|float $amount
     */
    protected $amount = 0;

    /**
     * @return string
     * @throws ProductDoesNotHaveSkuException
     */
    public function getSku(): string
    {
        if($this->sku == false){
            throw new ProductDoesNotHaveSkuException();
        }

        return $this->sku;
    }

    /**
     * @param String $sku
     * @return ProductInterface
     */
    public function setSku(String $sku): ProductInterface
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @param bool $on
     * @return ProductInterface
     */
    public function setActiveDiscount(bool $on = true): ProductInterface
    {
        $this->activeDiscount = $on;

        return $this;
    }

    /**
     * @param int $quantity
     * @return ProductInterface
     * @throws IllegalNegativeValueException
     */
    public function setDiscountThreshold(int $quantity = 1): ProductInterface
    {
        if($quantity<0){
            throw new IllegalNegativeValueException();
        }

        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @param float $amount
     * @return ProductInterface
     * @throws IllegalNegativeValueException
     */
    public function setDiscountAmount(float $amount = 0): ProductInterface
    {
        if($amount<0){
            throw new IllegalNegativeValueException();
        }

        $this->amount = $amount;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasActiveDiscount(): bool
    {
        return $this->activeDiscount;
    }

    /**
     * @return int
     */
    public function getDiscountThreshold(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getDiscountAmount(): float
    {
        return floatval($this->amount);
    }

    /**
     * @param int $price
     * @return ProductInterface
     * @throws IllegalNegativeValueException
     */
    public function setPrice($price = 0): ProductInterface
    {
        if($price<0){
            throw new IllegalNegativeValueException();
        }

        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice() :float
    {
        return floatval($this->price);
    }
}