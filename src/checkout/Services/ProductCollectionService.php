<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 04:34
 */

namespace Anarchalien\Checkout\Services;

use Anarchalien\Checkout\Interfaces\ProductCollectionInterface;
use Anarchalien\Checkout\Interfaces\ProductFactoryInterface;

/**
 * Class ProductCollectionService
 * @package Anarchalien\Checkout\Services
 */
class ProductCollectionService implements ProductCollectionInterface
{
    /**
     * @var array
     */
    protected $products = [];

    /**
     * ProductCollectionService constructor.
     * @param ProductFactoryInterface $factory
     */
    public function __construct(ProductFactoryInterface $factory)
    {
        $this->products = $factory::getProducts();
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->products;
    }

    /**
     * @param string $sku
     * @return array
     */
    public function findBySku(string $sku): array
    {
        $results = [];

        foreach ($this->products as $product){
            if($product->getSku()==$sku){
                $results[] = $product;
            }
        }

        return $results;
    }
}