<?php

namespace Anarchalien\Checkout\Exceptions;

use \Exception;

/**
 * Class AbstractCheckoutException
 * @package Anarchalien\Checkout\Exceptions
 */
abstract class AbstractCheckoutException extends Exception
{
    const NOT_EXIST = 'Product Does Not Exist';

    const SKU_NOT_SET = 'Sku not set';

    const NEGATIVE_VALUE = 'Value cannot be negative';
}