<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 31/05/18
 * Time: 17:09
 */

namespace Anarchalien\Checkout\Exceptions;

/**
 * Class NonExistentProductException
 * @package Anarchalien\Checkout\Exceptions
 */
class NonExistentProductException extends AbstractCheckoutException
{
    /**
     * @var string
     */
    protected $message = AbstractCheckoutException::NOT_EXIST;
}