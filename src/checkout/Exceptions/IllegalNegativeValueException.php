<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 04:16
 */

namespace Anarchalien\Checkout\Exceptions;

/**
 * Class IllegalNegativeValueException
 * @package Anarchalien\Checkout\Exceptions
 */
class IllegalNegativeValueException extends AbstractCheckoutException
{
    /**
     * @var string
     */
    protected $message = AbstractCheckoutException::NEGATIVE_VALUE;
}