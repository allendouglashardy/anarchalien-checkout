<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 31/05/18
 * Time: 17:12
 */

namespace Anarchalien\Checkout\Exceptions;

/**
 * Class ProductDoesNotHaveSkuException
 * @package Anarchalien\Checkout\Exceptions
 */
class ProductDoesNotHaveSkuException extends AbstractCheckoutException
{
    /**
     * @var string
     */
    protected $message = AbstractCheckoutException::SKU_NOT_SET;
}