<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 06:56
 */

namespace Anarchalien\Checkout;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Anarchalien\Checkout\Services\CartService;
use Anarchalien\Checkout\Factories\ProductFactory;
use Anarchalien\Checkout\Services\ProductCollectionService;
use Symfony\Component\Console\Helper\Table;

/**
 * Class Console
 * @package Anarchalien\Checkout
 */
class Console extends Command
{
    protected $cart;

    public function __construct($name = null)
    {
        $this->cart = (new CartService(
            new ProductCollectionService(
                new ProductFactory()
            )
        ));

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('app:checkout')
            ->setDescription('Simulates a checkout')
            ->setHelp('Enter a string of SKUs to get totals')
            ->addArgument('skus',InputArgument::REQUIRED,'String of skus');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        try{
            $skus = $input->getArgument('skus');

            $skus = str_split($skus);
            foreach ($skus as $sku){
                $this->cart->addItem($sku);
            }

            $total = $this->cart->getTotals();

            $table = new Table($output);
            $table->setHeaders([
                'SKU','Quantity','Total'
            ])
            ->setRows(
                $this->cart->getBill()
            )->render();
        }
        catch(\Exception $e){
            $output->writeln($e->getMessage());
        }
    }
}