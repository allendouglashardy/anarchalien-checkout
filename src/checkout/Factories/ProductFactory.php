<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 04:40
 */

namespace Anarchalien\Checkout\Factories;

use Anarchalien\Checkout\Interfaces\ProductFactoryInterface;
use Anarchalien\Checkout\Services\ProductService;
/**
 * Class ProductFactory
 * @package Anarchalien\Checkout\Factories
 */
class ProductFactory implements ProductFactoryInterface
{
    /**
     * @return array
     */
    public static function getProducts():array
    {
        return [
            (new ProductService())->setSku('A')->setPrice(50)
                ->setActiveDiscount(true)
                ->setDiscountThreshold(3)
                ->setDiscountAmount(20),
            (new ProductService())->setSku('B')->setPrice(30)
                ->setActiveDiscount(true)
                ->setDiscountThreshold(2)
                ->setDiscountAmount(15),
            (new ProductService())->setSku('C')->setPrice(20),
            (new ProductService())->setSku('D')->setPrice(15)
        ];
    }
}