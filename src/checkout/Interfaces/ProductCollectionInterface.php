<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 04:31
 */

namespace Anarchalien\Checkout\Interfaces;

/**
 * Interface ProductCollectionInterface
 * @package Anarchalien\Checkout\Interfaces
 */
interface ProductCollectionInterface
{
    /**
     * @return array
     */
    public function all() : array;

    /**
     * @param string $sku
     * @return array
     */
    public function findBySku(string $sku) : array;
}