<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 01/06/18
 * Time: 05:02
 */

namespace Anarchalien\Checkout\Interfaces;

/**
 * Interface ProductFactoryInterface
 * @package Anarchalien\Checkout\Interfaces
 */
interface ProductFactoryInterface
{
    /**
     * @return array
     */
    public static function getProducts():array;
}