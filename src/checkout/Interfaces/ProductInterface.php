<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 31/05/18
 * Time: 16:50
 */

namespace Anarchalien\Checkout\Interfaces;

/**
 * Interface ProductInterface
 * @package Anarchalien\Checkout\Interfaces
 */
interface ProductInterface
{
    /**
     * @return string
     */
    public function getSku():string;

    /**
     * @param String $sku
     * @return ProductInterface
     */
    public function setSku(String $sku) : ProductInterface;

    /**
     * @param bool $on
     * @return ProductInterface
     */
    public function setActiveDiscount(bool $on = true) : ProductInterface;

    /**
     * @param int $quantity
     * @return ProductInterface
     */
    public function setDiscountThreshold(int $quantity=1) : ProductInterface;

    /**
     * @param float $amount
     * @return ProductInterface
     */
    public function setDiscountAmount(float $amount=0): ProductInterface;

    /**
     * @return bool
     */
    public function hasActiveDiscount() : bool;

    /**
     * @return int
     */
    public function getDiscountThreshold() : int;

    /**
     * @return float
     */
    public function getDiscountAmount() : float;

    /**
     * @param int $price
     * @return ProductInterface
     */
    public function setPrice($price=0) : ProductInterface;
}