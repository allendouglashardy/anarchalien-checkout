<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 31/05/18
 * Time: 18:25
 */

namespace Anarchalien\Checkout\Interfaces;

/**
 * Interface CartInterface
 * @package Anarchalien\Checkout\Interfaces
 */
interface CartInterface
{
    /**
     * @return array
     */
    public function getItems() : array;

    /**
     * @return array
     */
    public function getBill() :array;

    /**
     * @return float
     */
    public function getTotals(): float;

    /**
     * @param string $sku
     * @param int $quantity
     * @return CartInterface
     */
    public function addItem(string $sku,int $quantity=1):CartInterface;
}