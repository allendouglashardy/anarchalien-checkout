# README #

How to use


### What is this repository for? ###

* Checkout demo

### How do I get set up? ###

* Requires Php 7.2
* Run `composer update` in project route directory to install dependencies

## Commands

* run php console.php for a list of commands
* run php console.php app:checkout to run the checkout command, supply a quoted string listing skus e.g: 'ABCABBB'

## Tests

* Makes use of PhpUnit for tests.
* You can run the testsuite by typing `vendor/bin/phpunit tests` into your terminal 

### Who do I talk to? ###

allen.hardy@myself.com
